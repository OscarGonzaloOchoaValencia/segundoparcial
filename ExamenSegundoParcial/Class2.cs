﻿
using System;
using System.IO;
namespace readwriteapp
{
    class Class2
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                
                StreamWriter sw = new StreamWriter("C:\\EjemploTexto.txt");
              
                sw.WriteLine("Hello World!!");

                sw.WriteLine("From the StreamWriter class");
                
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }
    }
}